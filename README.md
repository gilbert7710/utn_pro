# TECNOLOGÍA UTILIZADA #

* BE: NodeJS, Sequelize ORM 
* FE: AngularJS, Express
* BD: MySQL
* Repository: Git

# INSTALAR Y CONFIGURAR MYSQL #

## Paso#1: Installar MySQL Workbench 8.0CE

## Paso#2: Crear usuario (En MySQL) para la conexión

CREATE USER 'user'@'localhost'IDENTIFIED WITH mysql_native_password BY 'password';

## Paso#3: Asignar todos los permisos y privilegios al usuario creado

GRANT ALL PRIVILEGES ON database.* TO 'user'@'localhost';

## Paso#4: Logearse con usuario creado y crear Base de Datos.

    database: 'emotion',


# EJECUTAR EL PROYECTO (DESP. DE SU CLONACIÓN)#

## Paso#1: Ir a /utn_pro. Por medio de la consola (git bash) instalar npm, gulp y bower ejecutando los sgtes comandos.

	npm install
	
	npm install -g bower
	
	npm install gulp --global
	
	bower install

## Paso#2: Levantar el Servidor (BE). Consola 1. Ruta: utn_pro/

	npm start

## Paso#3: Sequelize le creará las tablas, pero debe agregar el usuario y asignarle 1 rol (EN MYSQL) para poder logearse.

	insert into user values (1, 'admin', 123, true, '2020-04-25 17:12:38', '2020-04-25 17:12:38',1);

	insert into rol_user_list values ('2020-04-25 17:12:38', '2020-04-25 17:12:38', 1, 1);

## Paso#8: Levantar el Cliente (FE). Consola 2. Ruta utn_pro/client

	gulp serve

# CREAR DEPLOY PARA PROD.
gulp 'to build an optimized version of your application in folder dist'


# OTROS COMANDOS DE GULP
	gulp serve:dist 'to start BrowserSync server on your optimized application without live reload'
	gulp test 'to run your unit tests with karma'
	gulp test:auto 'to run your unit tests with karma in watch mode'
	gulp protractor 0to launch your e2e tests with Protractor'
	gulp protractor:dist 'to launch your e2e tests with Protractor on the dist files'

more information:
https://github.com/Swiip/generator-gulp-angular/tree/master/docs


# Configuración con servidor Nginx
	Navegar a la dirección etc/nginx/sites-available/
	modificar el archivo default
	nano default 'permite editar el archivo default'

server {
	listen 80;
	listen [::]:80;
	
	root /var/www/"yourfolder";
	index index.html;
	
	location /api {
	
		rewrite /api/(.*) /$1 break;
		proxy_pass ttp://localhost:3000;
		proxy_redirect off;
		proxy_set_header Host $host;
	}
}


# Configuraciones del servidor linux
	sudo apt update 'Actualiza los nombres del servidor'
	sudo apt upgrate 'Actualiza el servidor'

# PM2
(Herramienta para mantener la apliación activa en el servidor)
	pm2 start index.js 'Ejecuta la app'
	pm2 startup systemd 'Se ejecuta la app automaticamente si el servidor se reinicia'
	pm2 list 'Lista los servicios'
	pm2 stop "name" 'detiene el servicio'

	sudo systemctl status pm2-root 'revisa si el servivio eta activo'
	sudo apt install nginx 'instala nginx'
