module.exports = {
  development: {
    dialect: "mysql",
    //storage: "./db.development.sqlite"
  },
  test: {
    dialect: "mysql",
    //storage: ":memory:"
  },
  production: {
    username: 'gamboa',
    password: '123456',
    database: 'emotion',
    host: 'localhost',
    dialect: 'mysql',
    use_env_variable: false
  }
};